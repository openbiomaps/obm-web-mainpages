<h2><?php echo t(str_project_statistics)?></h2>


<?php

$mf = new mainpage_functions();

if (isset($MAINPAGE_VARS['sidebar1']))
    $boxes = preg_split('/\|/',$MAINPAGE_VARS['sidebar1']);

else
    $boxes = array('members','uploads','data','species','species_stat');



if (in_array('members',$boxes)) {
    echo "
<div class='leftbox boxborder'><h3>". t(str_members) ."</h3>
    <ul class='boxul'>
    <li class='blarge'>"; 
    echo $mf->count_members(); 
    echo "</li>
    </ul>
</div>";
}

if (in_array('uploads',$boxes)) {
    $mf->count_uploads_cache = 5;
    echo "
<div class='leftbox boxborder'><h3>". t(str_uploads) ."</h3>
    <ul class='boxul'>
        <li class='blarge'>"; 
    echo $mf->count_uploads();
    echo "</li>
    </ul>
</div>";

}

if (in_array('data',$boxes)) {
    $mf->table = defined('DEFAULT_TABLE') ? DEFAULT_TABLE : PROJECTTABLE ;
    echo "
<div class='leftbox boxborder'><h3>". t(str_data) ."</h3>
    <ul class='boxul'>
        <li class='blarge'>";
    echo $mf->count_data(); 
    echo "</li>
    </ul>
</div>";
}

if (in_array('species',$boxes)) {
    $mf->count_species_cache = 5;
    echo "
<div class='leftbox boxborder'><h3>". t(str_species) ."</h3>
    <ul class='boxul'>
        <li class='blarge'><a href='?specieslist'>";
    echo $mf->count_species(true); 
    echo "</a></li>
    </ul>
</div>";
}

if (in_array('species_stat',$boxes)) {
    echo "
<div class='leftbox boxborder'><h3>". t(str_species_stat) ."</h3>
    <ul class='boxul'>
        <li>";
    echo $mf->stat_species();
    echo "</li>
    </ul>
</div>";
}
?>
