<?php

# 
# Official grid based mainpage tempalte
# Customizable
# Put your mainpage files into the private direcotry
#
#

if (file_exists($MAINPAGE_PATH."private/mainpage_grid.php"))
    require_once($MAINPAGE_PATH."private/mainpage_grid.php");
else
    require_once($MAINPAGE_PATH."mainpage_grid.php");

if (file_exists($MAINPAGE_PATH."private/mainpage_functions.php"))
    require_once($MAINPAGE_PATH.'private/mainpage_functions.php');
else 
    require_once($MAINPAGE_PATH.'mainpage_functions.php');


$mgrid = new mgrid();

echo "<script>$(document).ready(function(){});</script>";
echo "<style>.wrapper {grid-gap:".$mgrid->gap.";grid-template-areas:".area($mgrid->areas).";}</style>";
echo "<style>@media only screen and (min-width: 600px) {.wrapper {grid-gap:".$mgrid->gap600.";grid-template-columns:".$mgrid->columns600.";grid-template-areas:".area($mgrid->areas600).";}}</style>";
echo "<style>@media only screen and (min-width: 900px) {.wrapper {grid-gap:".$mgrid->gap900.";grid-template-columns:".$mgrid->columns900.";grid-template-areas:".area($mgrid->areas900).";}}</style>";
echo "<style>@media only screen and (min-width: 1400px) {.wrapper {grid-gap:".$mgrid->gap1400.";grid-template-columns:".$mgrid->columns1400.";grid-template-areas:".area($mgrid->areas1400).";}}</style>";

foreach ($mgrid->elements as $mii ) {

    if (file_exists($MAINPAGE_PATH."private/mainpage_$mii.php")) {
        ob_start();
        include_once($MAINPAGE_PATH."private/mainpage_$mii.php");
        $content = ob_get_contents();
        $cellc = $content;
        ob_end_clean();
        $mgrid->definition = preg_replace("/#$mii/",$cellc,$mgrid->definition);
    }
    else {
        ob_start();
        include_once($MAINPAGE_PATH."mainpage_$mii.php");
        $content = ob_get_contents();
        $cellc = $content;
        ob_end_clean();
        $mgrid->definition = preg_replace("/#$mii/",$cellc,$mgrid->definition);
    }
}
echo $mgrid->definition;

/*if (!isset($mgrid->$cell))
    return;
$def = $mgrid->$cell;

$bgcol = '';
$rowspan = '';
$colspan = '';
$cellc = '';
$classes = '';

if (isset($def['bg']) and $def['bg'] == 'randomcolor')
    $bgcol = "background-color:#".rand_color();
elseif (isset($def['bg']) and $def['bg'] == 'color')
    $bgcol = "background-color:{$def['bgcolor']}";

if (isset($def['rowspan']) and $def['rowspan'])
    $rowspan = "rowspan='{$def['rowspan']}'";

if (isset($def['colspan']) and $def['colspan'])
    $rowspan = "colspan='{$def['colspan']}'";

if (isset($def['class']) and $def['class']!='')
    $classes = $def['class'];

if (file_exists("includes/$cell.php")) {
    ob_start();
    include_once("includes/$cell.php");
    $content = ob_get_contents();
    $cellc = $content;
    ob_end_clean();
    //return "<td style='$bgcol' class='$classes' $rowspan $colspan id='$cell'>$cellc</td>";
    return "<div style='$bgcol' class='box $classes' id='$cell'>$cellc</div>";
}*/

?>
