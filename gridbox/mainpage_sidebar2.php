<?php

    if (!isset($_SESSION['Tid']))
        $out = login_box();
    else {

        $mf = new mainpage_functions();
        $out = "<h2>".t(str_user_statistics)."</h2>";
        $dobj = json_decode(get_profile_data(PROJECTTABLE,$_SESSION['Tcrypt']));

        $out .= "<div class='downbox'><h3>".str_uploadcount."</h3>";
        $out .= "<ul class='boxul'>";
        $out .= sprintf("<li class='blarge'>%d</li>",$dobj->{'uplcount'});
        $out .= "</ul></div>";

        $out .= "<div class='downbox'><h3>".str_modcount."</h3>";
        $out .= "<ul class='boxul'>";
        $out .= sprintf("<li class='blarge'>%d</li>",$dobj->{'modcount'});
        $out .= "</ul></div>";

        $out .= "<div class='downbox'><h3>".str_rowcount."</h3>";
        $out .= "<ul class='boxul'>";
        $out .= sprintf("<li class='blarge'>%d</li>",$dobj->{'upld'});
        $out .= "</ul></div>";

        $out .= "<div class='downbox'><h3>".str_valuations."</h3>";
        $out .= "<ul class='boxul'>";
        $out .= sprintf("<li class='blarge'>%d</li>",$dobj->{'validation'});
        $out .= "</ul></div>";

        $out .= "<div class='downbox'><h3>".str_most_frequent_species."</h3>";
        $out .= "<ul class='boxul'>";
        $out .= sprintf("<li>%s</li>",$mf->stat_species_user());
        $out .= "</ul></div>";


    }
    echo $out;

?>
